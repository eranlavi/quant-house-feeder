﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Configuration;

using FeedOSAPI.Types;
using FeedOSManaged;
using System.Xml;
using System.Collections.Concurrent;

namespace QuantHouseFeeder
{
    public class QuantHouse
    {
        string s_DatePattern = "yyyy-MM-dd";
        string s_TimeSecPattern = "HH:mm:ss:fff";
        string s_TimeMicrosecPattern = "HH:mm:ss:ffffff";
        string s_ShortDateTimePattern;
        string s_FullDateTimePattern;
        private long s_OriginOfTime_CE = 621355968000000000;
        const ushort TRADING_STATUS_ID = FeedOSAPI.Types.TagsQuotation.TAG_TradingStatus;

        bool Reconnect;
        CultureInfo m_CultureInfo;
        Connection connection;
       
        Dictionary<uint, string> OutputNames;

        private class LastSymbolQuote
        {
            public double Bid;
            public double Ask;
        }
        static ConcurrentDictionary<uint, LastSymbolQuote> Lastquote;

        UnifeedServer ufs;

        public event Action<bool> ReconnectEvent;

        public QuantHouse()
        {
            Reconnect = true;
            OutputNames = new Dictionary<uint, string>();
            m_CultureInfo = (CultureInfo)CultureInfo.InvariantCulture.Clone();
            Lastquote = new ConcurrentDictionary<uint, LastSymbolQuote>();

            s_ShortDateTimePattern = s_DatePattern + " " + s_TimeSecPattern;
            s_FullDateTimePattern = s_DatePattern + " " + s_TimeMicrosecPattern;

            ufs = new UnifeedServer();
            ufs.Start();
        }

        public void Start()
        {
            Tracer.GetTracer().Info("Starting API");

            FeedOSManaged.API.Init(ConfigurationManager.AppSettings["ApiApplicationName"]);

            API.InitTraceAllAreas(ConfigurationManager.AppSettings["ApiLogFile"],
                    true,  // append_mode
                    true,   // flush_every_line
                    true,   // enable_error_level
                    true,   // enable_warning_level
                    true,   // enable_info_level
                    false,   // enable_debug_level 
                    false    // enable_scope_level 
                );
            
            connection = new Connection();
                        
            connection.Disconnected += new FeedOSManaged.DisconnectedEventHandler(connection_Disconnected);
            connection.AdminMessage += new FeedOSManaged.AdminMessageEventHandler(connection_AdminMessage);
            connection.SnapshotsHandler += new FeedOSManaged.SnapshotsEventHandler(SnapshotsHandler);
            connection.TradeEventExtHandler += new FeedOSManaged.TradeEventExtEventHandler(TradeEventExtHandler);
            
            //connection.HeartBeat += new FeedOSManaged.HeartBeatEventHandler(connection_HeartBeat);
            //connection.MsgDispatchingHookBegin += new FeedOSManaged.MsgDispatchingHookBeginEventHandler(connection_MsgDispatchingHookBegin);
            //connection.MsgDispatchingHookEnd += new FeedOSManaged.MsgDispatchingHookEndEventHandler(connection_MsgDispatchingHookEnd);
            //connection.InstrumentsCreatedHandler += new FeedOSManaged.InstrumentsCreatedEventHandler(connection_InstrumentsCreatedHandler);
            //connection.InstrumentsHandler += new FeedOSManaged.InstrumentsEventHandler(connection_InstrumentsHandler);                                 
            //connection.ValuesUpdateHandler += new FeedOSManaged.ValuesUpdateEventHandler(ValuesUpdateHandler);

            uint returnCode = connection.Connect(ConfigurationManager.AppSettings["ApiHostIP"], Convert.ToUInt32(ConfigurationManager.AppSettings["ApiHostPort"]),
                ConfigurationManager.AppSettings["ApiUsername"], ConfigurationManager.AppSettings["ApiPassword"]);
            if (returnCode == 0)
            {
                //connection.SubscribeToFeedStatus(2);
                List<string> m_PolymorphicInstrumentCodeList = new List<string>();

                XmlDocument xDoc = new XmlDocument();
                xDoc.Load("Symbols.xml");
                XmlNodeList nodes = xDoc.SelectNodes("//symbols/symbol");

                OutputNames.Clear();
                foreach (XmlNode node in nodes)
                {
                    OutputNames.Add(Convert.ToUInt32(node.Attributes["Code"].Value), node.Attributes["out"].Value);
                    m_PolymorphicInstrumentCodeList.Add(node.Attributes["RegistrationName"].Value);
                }
                
                connection.SubscribeL1(m_PolymorphicInstrumentCodeList, null, QuotationContent.Mask_EVERYTHING, false, 1);
            }
            else
            {                
                Tracer.GetTracer().Error("API connection failed: " + FeedOSManaged.API.ErrorString(returnCode));
            }            
        }

        public void Stop()
        {
            Reconnect = false;

            try
            {
                connection.Disconnect();
            }
            catch { }
            connection.Disconnected -= new FeedOSManaged.DisconnectedEventHandler(connection_Disconnected);
            connection.AdminMessage -= new FeedOSManaged.AdminMessageEventHandler(connection_AdminMessage);
            connection.SnapshotsHandler -= new FeedOSManaged.SnapshotsEventHandler(SnapshotsHandler);
            connection.TradeEventExtHandler -= new FeedOSManaged.TradeEventExtEventHandler(TradeEventExtHandler);

            try
            {
                FeedOSManaged.API.Shutdown();
            }
            catch { }
        }

        private string ToFastDateTimeString()
        {
            DateTime Date = DateTime.Now;
            char[] chars = new char[23];
            chars[0] = Digit(Date.Day / 10);
            chars[1] = Digit(Date.Day % 10);
            chars[2] = '/';
            chars[3] = Digit(Date.Month / 10);
            chars[4] = Digit(Date.Month % 10);
            chars[5] = '/';
            chars[6] = Digit(Date.Year / 1000);
            chars[7] = Digit(Date.Year % 1000 / 100);
            chars[8] = Digit(Date.Year % 100 / 10);
            chars[9] = Digit(Date.Year % 10);
            chars[10] = '-';
            chars[11] = Digit(Date.Hour / 10);
            chars[12] = Digit(Date.Hour % 10);
            chars[13] = ':';
            chars[14] = Digit(Date.Minute / 10);
            chars[15] = Digit(Date.Minute % 10);
            chars[16] = ':';
            chars[17] = Digit(Date.Second / 10);
            chars[18] = Digit(Date.Second % 10);
            chars[19] = '.';
            chars[20] = Digit(Date.Millisecond / 100);
            chars[21] = Digit(Date.Millisecond % 100 / 10);
            chars[22] = Digit(Date.Millisecond % 10);

            return new string(chars);
        }
        private char Digit(int value) { return (char)(value + '0'); }

        private string DumpInstrument(uint instrumentCode)
        {
            return (FeedOSManaged.API.FOSMarketIDInstrument(instrumentCode) + "/" + FeedOSManaged.API.LocalCodeInstrument(instrumentCode));
        }

        private string DumpTimestamp(DateTime timestamp, bool full)
        {
            string format = full ? s_FullDateTimePattern : s_ShortDateTimePattern;
            return (s_OriginOfTime_CE != ((DateTime)timestamp).Ticks) ? timestamp.ToString(format) : "(null)";
        }

        private void TradeEventExtHandler(uint requestId, uint instrumentCode, ValueType serverUTCDateTime, ValueType marketUTCDateTime, QuotationTradeEventExt quotationTradeEventExt)
        {
            //Console.WriteLine("EV " + DumpInstrument(instrumentCode) + " " +
            //                    DumpTimestamp((DateTime)marketUTCDateTime, false) + " /ServerTime: " +
            //                    DumpTimestamp((DateTime)serverUTCDateTime, true));

            double bid = 0.0;
            double ask = 0.0;

            if (quotationTradeEventExt.BestBid != null)
            {
                //Console.WriteLine(quotationTradeEventExt.BestBid.DumpOneSideLimit("Bid", m_CultureInfo));
                bid = quotationTradeEventExt.BestBid.Price;
            }

            if (quotationTradeEventExt.BestAsk != null)
            {
                //Console.WriteLine(quotationTradeEventExt.BestAsk.DumpOneSideLimit("Ask", m_CultureInfo));
                ask = quotationTradeEventExt.BestAsk.Price;
            }

            bool send = false;
            LastSymbolQuote lastSymbolQuote;
            if (!Lastquote.TryGetValue(instrumentCode, out lastSymbolQuote))
            {
                lastSymbolQuote = new LastSymbolQuote();
                lastSymbolQuote.Ask = ask;
                lastSymbolQuote.Bid = bid;

                Lastquote.AddOrUpdate(instrumentCode, lastSymbolQuote, (k, v) => v = lastSymbolQuote);
            }
            else
            {

                if (lastSymbolQuote.Bid != bid && bid > 0.0)
                {
                    send = true;
                    lastSymbolQuote.Bid = bid;
                    Lastquote.AddOrUpdate(instrumentCode, lastSymbolQuote, (k, v) => v = lastSymbolQuote);
                }
                if (lastSymbolQuote.Ask != ask && ask > 0.0)
                {
                    send = true;
                    lastSymbolQuote.Ask = ask;
                    Lastquote.AddOrUpdate(instrumentCode, lastSymbolQuote, (k, v) => v = lastSymbolQuote);
                }
            }

            if (send && bid > 0.0 && ask > 0.0)
            {
                string name;
                if(OutputNames.TryGetValue(instrumentCode, out name))
                    ufs.SendData(name + " " + ToFastDateTimeString() + " " + bid.ToString() + " " + ask.ToString() + "\r\n");
            }

            //if (QuotationContentMaskHelper.contains(quotationTradeEventExt.ContentMask, QuotationContentMaskHelper.QuotationContentBit_LastPrice))
            //{
            //    Console.WriteLine("        LastPrice     = " + quotationTradeEventExt.Price);
            //}            
        }

        private void SnapshotsHandler(uint requestId, List<InstrumentStatusL1> instrumentStatusL1)
        {            
            if (null != instrumentStatusL1)
            {
                foreach (InstrumentStatusL1 statusL1 in instrumentStatusL1)
                {                    
                    Tracer.GetTracer().Info("Subscribed to instrument id: " + statusL1.InstrumentCode.ToString());                    
                }
            }
        }

        void connection_AdminMessage(bool isUrgent, string origin, string headline, string content)
        {
            Tracer.GetTracer().Info("API Admin Message - Origin: " + origin + " , Headline" + headline + " , Content" + content);
        }

        void connection_Disconnected(uint returnCode)
        {
            if (!Reconnect)
                return;

            Tracer.GetTracer().Error("API Disconnected Event - Code: " + returnCode.ToString() + " , Description: " + FeedOSManaged.API.ErrorString(returnCode));
            if (ReconnectEvent != null)
                ReconnectEvent(true);
        }

        

        //private void ValuesUpdateHandler(uint requestId, uint instrumentCode, ValueType marketUTCDateTime, QuotationValuesUpdate quotationValuesUpdate)
        //{

        //}

        //void connection_MsgDispatchingHookEnd()
        //{

        //}

        //void connection_MsgDispatchingHookBegin(uint nbPendingMsg)
        //{

        //}

        //void connection_HeartBeat(ValueType serverUTCDateTime, uint heartbeatPeriodSec)
        //{

        //}

        //void connection_InstrumentsHandler(uint requestId, List<InstrumentCharacteristics> instrumentCharacteristicsList)
        //{

        //}

        //void connection_InstrumentsCreatedHandler(uint requestId, List<InstrumentCharacteristics> instrumentCharacteristicsList)
        //{

        //}

        


    }
}
