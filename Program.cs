﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

namespace QuantHouseFeeder
{
    class Program
    {
        static QuantHouse quantHouse;

        static void Main(string[] args)
        {
            String assemblyLocationFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (String.Compare(Environment.CurrentDirectory, assemblyLocationFolder, StringComparison.OrdinalIgnoreCase) != 0)
            {
                Environment.CurrentDirectory = assemblyLocationFolder;
            }

            try
            {
                System.IO.Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings["BaseLogsDirectory"]);
            }
            catch { }

            Tracer.GetTracer().Info("Starting QuantHouseFeeder");

            quantHouse = new QuantHouse();
            quantHouse.ReconnectEvent += quantHouse_ReconnectEvent;

            System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(quantHouse.Start));
            thread.Start(); 
                                
            Console.ReadKey();

            quantHouse.Stop();
        }

        static void quantHouse_ReconnectEvent(bool obj)
        {
            quantHouse.Stop();

            System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(quantHouse.Start));
            thread.Start(); 
        }
    }
}
