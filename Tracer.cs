﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Configuration;

namespace QuantHouseFeeder
{
    public sealed class Tracer
    {
        private string ToFastDateTimeString(DateTime Date)
        {
            char[] chars = new char[24];
            chars[0] = Digit(Date.Day / 10);
            chars[1] = Digit(Date.Day % 10);
            chars[2] = '/';
            chars[3] = Digit(Date.Month / 10);
            chars[4] = Digit(Date.Month % 10);
            chars[5] = '/';
            chars[6] = Digit(Date.Year / 1000);
            chars[7] = Digit(Date.Year % 1000 / 100);
            chars[8] = Digit(Date.Year % 100 / 10);
            chars[9] = Digit(Date.Year % 10);
            chars[10] = '-';
            chars[11] = Digit(Date.Hour / 10);
            chars[12] = Digit(Date.Hour % 10);
            chars[13] = ':';
            chars[14] = Digit(Date.Minute / 10);
            chars[15] = Digit(Date.Minute % 10);
            chars[16] = ':';
            chars[17] = Digit(Date.Second / 10);
            chars[18] = Digit(Date.Second % 10);
            chars[19] = '.';
            chars[20] = Digit(Date.Millisecond / 100);
            chars[21] = Digit(Date.Millisecond % 100 / 10);
            chars[22] = Digit(Date.Millisecond % 10);
            chars[23] = ' ';

            return new string(chars);
        }
        private char Digit(int value) { return (char)(value + '0'); }

        private static readonly Tracer _instance =
          new Tracer();

        private TraceSource ts = new TraceSource("TraceSource");
        private System.DateTime _currentDate;
        private string _fileName;
        private object checkRolloverLock;

        private Tracer()
        {
            checkRolloverLock = new object();
            var listener = (TextWriterTraceListener)ts.Listeners["textwriterListener"];
            var writer = (StreamWriter)listener.Writer;
            var stream = (FileStream)writer.BaseStream;
            _fileName = stream.Name;

            CreateNewTracer();

            try
            {
                System.IO.File.Delete(_fileName);
            }
            catch { }
        }

        public static Tracer GetTracer()
        {
            return _instance;
        }

        public void Debug(string msg)
        {
            checkRollover();
            ts.TraceEvent(TraceEventType.Verbose, 0, "\t" + ToFastDateTimeString(DateTime.Now) + msg);
        }

        public void Debug(string msg, Exception ex)
        {
            checkRollover();
            ts.TraceEvent(TraceEventType.Verbose, 0, "\t" + ToFastDateTimeString(DateTime.Now) + msg + " " + ex.Message + "\r\n" + ex.StackTrace);
        }

        public void Info(string msg)
        {
            checkRollover();
            ts.TraceEvent(TraceEventType.Information, 0, "\t" + ToFastDateTimeString(DateTime.Now) + msg);
        }

        public void Info(string msg, Exception ex)
        {
            checkRollover();
            ts.TraceEvent(TraceEventType.Information, 0, "\t" + ToFastDateTimeString(DateTime.Now) + msg + " " + ex.Message + "\r\n" + ex.StackTrace);
        }

        public void Warnning(string msg)
        {
            checkRollover();
            ts.TraceEvent(TraceEventType.Warning, 0, "\t" + ToFastDateTimeString(DateTime.Now) + msg);
        }

        public void Warnning(string msg, Exception ex)
        {
            checkRollover();
            ts.TraceEvent(TraceEventType.Warning, 0, "\t" + ToFastDateTimeString(DateTime.Now) + msg + " " + ex.Message + "\r\n" + ex.StackTrace);
        }

        public void Error(string msg)
        {
            checkRollover();
            ts.TraceEvent(TraceEventType.Error, 0, "\t\t" + ToFastDateTimeString(DateTime.Now) + msg);
        }

        public void Error(string msg, Exception ex)
        {
            checkRollover();
            ts.TraceEvent(TraceEventType.Error, 0, "\t\t" + ToFastDateTimeString(DateTime.Now) + msg + " " + ex.Message + "\r\n" + ex.StackTrace);
        }

        private void checkRollover()
        {
            lock (checkRolloverLock)
            {
                if (_currentDate.CompareTo(DateTime.Today) != 0)
                {
                    CreateNewTracer();
                }
            }
        }

        private string generateFilename()
        {
            _currentDate = System.DateTime.Today;

            return Path.Combine(Path.GetDirectoryName(_fileName),
            Path.GetFileNameWithoutExtension(_fileName) + "_" +
            _currentDate.ToString("ddMMyyyy") + Path.GetExtension(_fileName));
        }

        private void CreateNewTracer()
        {
            try
            {
                var listener = (TextWriterTraceListener)ts.Listeners["textwriterListener"];

                listener.Close();
                listener.Dispose();

                ts.Listeners.Remove(listener);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("General_Error.txt", ToFastDateTimeString(DateTime.Now) + ex.Message + "\r\n" + ex.StackTrace + "\r\n");
            }

            try
            {
                ts.Listeners.Add(new TextWriterTraceListener(generateFilename(), "textwriterListener"));

            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("General_Error.txt", ToFastDateTimeString(DateTime.Now) + ex.Message + "\r\n" + ex.StackTrace + "\r\n");
            }
        }
    }
}
