﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Configuration;
using System.Threading;

namespace QuantHouseFeeder
{
    public class UnifeedServer
    {
        public class StateObject
        {
            public Socket workSocket = null;
            public const int BUFFER_SIZE = 1024;
            public byte[] buffer = new byte[BUFFER_SIZE];
            public string sb = string.Empty;
            public int WritePendingBytes = 0;
            public bool Valid = false;
            public string ClientIP = string.Empty;
            
        }

        public event Action<string> ServerNotification;
       
        int BackLog = 1024 * 10;
       
        ConcurrentDictionary<int, StateObject> connections = new ConcurrentDictionary<int, StateObject>();
        
        int MaxPendingWriteBufferToClient = Convert.ToInt32(ConfigurationManager.AppSettings["MaxPendingWriteBufferToClient"]);

        object locker;
        
        public UnifeedServer()
        {
            locker = new object();           
        }
        
        public void Start()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(StartServer), null);
        }

        void StartServer(Object stateInfo)
        {
            try
            {
                IPEndPoint localEP = new IPEndPoint(IPAddress.Any, Convert.ToInt32(ConfigurationManager.AppSettings["Port"]));

                Socket listener = new Socket(localEP.Address.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);

                listener.Bind(localEP);
                listener.Listen(BackLog);

                listener.BeginAccept(
                        new AsyncCallback(acceptCallback),
                        listener);
            }
            catch (Exception ex)
            {
                if (ServerNotification != null)
                    ServerNotification("StartServer() exception: " + ex.Message);
            }         
        }
                
        void acceptCallback(IAsyncResult ar)
        {
            // Get the socket that handles the client request.
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);

            try
            {
                StateObject so = new StateObject();
                so.ClientIP = IPAddress.Parse(((IPEndPoint)handler.RemoteEndPoint).Address.ToString()).ToString();

                if (ServerNotification != null)
                    ServerNotification("Got new connection from: " + so.ClientIP);
                

                handler.NoDelay = true;
                so.workSocket = handler;

                string auth = "Login: ";
                byte[] arr;
                int len;
                if (ConfigurationManager.AppSettings["Authenticate"] == "true")
                {
                    System.IO.StreamReader Reader = new System.IO.StreamReader(new NetworkStream(handler, false));

                    string[] d = System.IO.File.ReadAllLines("Security.txt");
                    byte[] r = new byte[128];
                    string ret;
                    handler.ReceiveTimeout = 10000;
                    
                    auth = "Login: ";
                    arr = System.Text.Encoding.ASCII.GetBytes(auth);                    
                    len = handler.Send(arr);
                    ret = Reader.ReadLine();
                    string u = string.Empty;
                    string p = string.Empty;
                    for (int i = 0; i < d.Length; i += 2)
                    {
                        if (d[i] == ret)
                        {
                            u = d[i];
                            p = d[i + 1];                            
                            break;
                        }
                    }

                    if (u == string.Empty)
                    {
                        handler.Close();
                        Reader.Close();
                        Reader.Dispose();
                        if (ServerNotification != null)
                            ServerNotification(so.ClientIP + " authentication failed: wrong username [" + ret + "]");                        
                        return;
                    }

                    auth = "Password: ";
                    arr = System.Text.Encoding.ASCII.GetBytes(auth);
                    len = handler.Send(arr);
                    ret = Reader.ReadLine();
                    if (p != ret)
                    {
                        handler.Close();
                        Reader.Close();
                        Reader.Dispose();
                        if (ServerNotification != null)
                            ServerNotification(so.ClientIP + " authentication failed: wrong password [" + ret + "]");                        
                        return;
                    }

                    auth = "Access granted";
                    arr = System.Text.Encoding.ASCII.GetBytes(auth);
                    len = handler.Send(arr);
                }
                else
                {
                    auth = "Login: ";
                    arr = System.Text.Encoding.ASCII.GetBytes(auth);
                    len = handler.Send(arr);                    
                    
                    auth = "Password: ";
                    arr = System.Text.Encoding.ASCII.GetBytes(auth);
                    len = handler.Send(arr);
                    
                    auth = "Access granted: ";
                    arr = System.Text.Encoding.ASCII.GetBytes(auth);
                    len = handler.Send(arr);
                }

                so.Valid = true;
                connections.TryAdd((int)so.workSocket.Handle, so);
                
                handler.BeginReceive(so.buffer, 0, StateObject.BUFFER_SIZE, 0,
                           new AsyncCallback(ReadCallback), so);

                if (ServerNotification != null)
                {
                    ServerNotification(so.ClientIP + " is Connected");
                    ServerNotification("Current active connections: " + connections.Count + " connections");
                }                                             
            }
            catch (System.Exception ex)
            {
                if (ServerNotification != null)
                    ServerNotification("acceptCallback() exception: " + ex.Message);

                try
                {                    
                    handler.Close();
                }
                catch { }                
            }
            finally
            {
                listener.BeginAccept(
                            new AsyncCallback(acceptCallback),
                            listener); 
            }
        }

        private bool IsClientConnected(Socket s)
        {
            bool ret = true;
            if ((s.Poll(1000, SelectMode.SelectRead) && s.Available == 0) || !s.Connected)
                ret = false;

            return ret;
        }

        void DisconnectClient(StateObject so)
        {
            try
            {
                if (!connections.TryRemove((int)so.workSocket.Handle, out so) || so == null)
                    return;
            }
            catch (System.Exception ex)
            {
                if (ServerNotification != null)
                    ServerNotification("DisconnectClient() exception on remove from list: " + ex.Message);
            }

            try
            {
                if (ServerNotification != null)
                {
                    ServerNotification("Client " + so.ClientIP + " disconnected");
                    ServerNotification("Current active connections: " + connections.Count + " connections");
                }
               
                so.workSocket.Close();
            }
            catch (System.Exception ex)
            {
                if (ServerNotification != null)
                    ServerNotification("DisconnectClient() exception on close socket: " + ex.Message);
            }
        }

        private void ReadCallback(IAsyncResult ar)
        {
            StateObject so = (StateObject)ar.AsyncState;
            Socket s = so.workSocket;

            try
            {                
                int read = s.EndReceive(ar);

                if (read > 0)
                {
                    so.sb = Encoding.ASCII.GetString(so.buffer, 0, read);                    
                }
                else
                {                    
                    so.Valid = false;
                                     
                    DisconnectClient(so);
                }
            }
            catch (System.Exception ex)
            {
                if (ServerNotification != null)
                    ServerNotification("ReadCallback() : " + ex.Message);

                DisconnectClient(so);
            }
            finally
            {
                try
                {
                    if (null != s && s.Connected && so.Valid)
                        s.BeginReceive(so.buffer, 0, StateObject.BUFFER_SIZE, 0, new AsyncCallback(ReadCallback), so);
                    else
                    {
                        if (null != s && s.Connected && so.Valid)
                            DisconnectClient(so);
                    }

                }
                catch
                {                    
                    DisconnectClient(so); 
                }
            }
        }
       
        public void SendData(string msgToSend)
        {           
            try
            {                
                byte[] bindata = System.Text.Encoding.ASCII.GetBytes(msgToSend);
                
                IEnumerator e = connections.GetEnumerator();
                while (e.MoveNext())
                {                    
                    StateObject so = ((KeyValuePair<int, StateObject>)e.Current).Value;
                    
                    if (so.WritePendingBytes >= MaxPendingWriteBufferToClient)
                    {
                        string drop = "Dropping client: " + so.ClientIP + " due to un-responsiveness";

                        if (ServerNotification != null)
                            ServerNotification(drop);
                        
                        so.Valid = false;
                        DisconnectClient(so);
                        continue;
                    }

                    if (so.Valid)
                    {
                        so.WritePendingBytes += bindata.Length;
                        try
                        {
                            so.workSocket.BeginSend(bindata, 0, bindata.Length, 0, new AsyncCallback(SendCallback), so);
                        }
                        catch { so.Valid = false; }
                    }
                    else
                        DisconnectClient(so);
                }
            }
            catch (System.Exception ex)
            {
                if (ServerNotification != null)
                    ServerNotification("SendData() exception: " + ex.Message);
            }               
        }

        
        private void SendCallback(IAsyncResult ar)
        {
            lock (locker)
            {
                StateObject so = (StateObject)ar.AsyncState;

                try
                {
                    // Complete sending the data to the remote device.
                    int bytesSent = so.workSocket.EndSend(ar);
                    so.WritePendingBytes -= bytesSent;
                }
                catch
                {
                    so.Valid = false;
                }
            }
        }


    }


}
